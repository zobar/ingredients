module Ingredients

  #
  # All exceptions raised by Ingredients are defined inside this namespace.
  #
  module Errors

    #
    # Raised by {Configuration::Root#set_defaults} if it detects that a role has
    # used `override_attributes` to set attributes for an Ingredients cookbook.
    #
    class RoleOverrideError < RuntimeError

      #
      # Provides a human-readable error message. By defining this here rather
      # than at the point of failure, we can standardize error messages and
      # declutter our methods a little.
      #
      # @return [String] A message describing this error.
      #
      def to_s
        "The #{super.inspect} cookbook is not compatible with role overrides."
      end
    end
  end
end
